package rekursion;

public class MyMath implements IMyMath {

	@Override
	public int fakultaet(int zahl) {
		int erg = 1;
		for (int i = 1; i <= zahl; i++) {
			erg = erg * i;
		}
		return erg;
	}

	@Override
	public int fibonacci(int zahl) {
		if (zahl == 0) {
			return 0;
		} else if (zahl == 1) {
			return 1;
		} else {
			return fibonacci(zahl - 1) + fibonacci(zahl - 2);
		}
	}
	
	public static void main(String[] args) {
		
		MyMath my = new MyMath();
		System.out.println(my.fakultaet(5)); //erg=120
		System.out.println(my.fibonacci(50)); //erg=120
		
	}

}
