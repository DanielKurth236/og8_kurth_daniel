import javax.swing.JPanel;
import java.awt.GridBagLayout;
import java.awt.Panel;
import java.awt.GridBagConstraints;
import java.awt.Canvas;
import java.awt.Insets;
import java.awt.Color;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JFrame;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

public class PokemonGUI extends JFrame {
	private final Component horizontalStrut_1 = Box.createHorizontalStrut(1);
	private final JButton btnNewButton_2 = new JButton("POWER-UP");

	/**
	 * Create the panel.
	 */
	public PokemonGUI() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBackground(new Color(102, 255, 255));
		setBounds(100, 100, 300, 700);
		
		//setBorder(new EmptyBorder(5, 5, 5, 5));
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] {270};
		gridBagLayout.rowHeights = new int[] {150};
		gridBagLayout.columnWeights = new double[]{1.0};
		gridBagLayout.rowWeights = new double[]{1.0};
		getContentPane().setLayout(gridBagLayout);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.CYAN);
		panel_1.setBorder(new EmptyBorder(5, 5, 0, 5));
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 0;
		getContentPane().add(panel_1, gbc_panel_1);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{264, 0};
		gbl_panel_1.rowHeights = new int[]{150, 563, 0};
		gbl_panel_1.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		Canvas canvas = new Canvas();
		GridBagConstraints gbc_canvas = new GridBagConstraints();
		gbc_canvas.fill = GridBagConstraints.BOTH;
		gbc_canvas.insets = new Insets(0, 0, 5, 0);
		gbc_canvas.gridx = 0;
		gbc_canvas.gridy = 0;
		panel_1.add(canvas, gbc_canvas);
		canvas.setBackground(new Color(204, 255, 255));
		
		Panel panel = new Panel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.anchor = GridBagConstraints.NORTH;
		gbc_panel.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 1;
		panel_1.add(panel, gbc_panel);
		panel.setBackground(Color.GRAY);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{276, 0};
		gbl_panel.rowHeights = new int[]{144, 129, 100, 31, 89, 0};
		gbl_panel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JPanel panel_Info = new JPanel();
		panel_Info.setBackground(Color.WHITE);
		GridBagConstraints gbc_panel_Info = new GridBagConstraints();
		gbc_panel_Info.insets = new Insets(0, 0, 5, 0);
		gbc_panel_Info.fill = GridBagConstraints.BOTH;
		gbc_panel_Info.gridx = 0;
		gbc_panel_Info.gridy = 0;
		panel.add(panel_Info, gbc_panel_Info);
		GridBagLayout gbl_panel_Info = new GridBagLayout();
		gbl_panel_Info.columnWidths = new int[]{49, 0, 0, 0, 0, 0};
		gbl_panel_Info.rowHeights = new int[]{0, 17, 0, 16, 0, 0, 0};
		gbl_panel_Info.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_panel_Info.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		panel_Info.setLayout(gbl_panel_Info);
		
		JLabel lblNewLabel_7 = new JLabel("Aerodactyl");
		lblNewLabel_7.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_lblNewLabel_7 = new GridBagConstraints();
		gbc_lblNewLabel_7.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_7.gridx = 2;
		gbc_lblNewLabel_7.gridy = 0;
		panel_Info.add(lblNewLabel_7, gbc_lblNewLabel_7);
		
		Panel panel_KPBar = new Panel();
		panel_KPBar.setBackground(Color.GREEN);
		GridBagConstraints gbc_panel_KPBar = new GridBagConstraints();
		gbc_panel_KPBar.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel_KPBar.insets = new Insets(0, 0, 5, 5);
		gbc_panel_KPBar.gridx = 2;
		gbc_panel_KPBar.gridy = 1;
		panel_Info.add(panel_KPBar, gbc_panel_KPBar);
		
		JLabel lblNewLabel_8 = new JLabel("KP 52/52");
		GridBagConstraints gbc_lblNewLabel_8 = new GridBagConstraints();
		gbc_lblNewLabel_8.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_8.gridx = 2;
		gbc_lblNewLabel_8.gridy = 2;
		panel_Info.add(lblNewLabel_8, gbc_lblNewLabel_8);
		
		JLabel lblNewLabel_9 = new JLabel("Gestein/Flug");
		lblNewLabel_9.setFont(new Font("Tahoma", Font.PLAIN, 11));
		GridBagConstraints gbc_lblNewLabel_9 = new GridBagConstraints();
		gbc_lblNewLabel_9.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_9.gridx = 1;
		gbc_lblNewLabel_9.gridy = 4;
		panel_Info.add(lblNewLabel_9, gbc_lblNewLabel_9);
		
		JLabel lblKg = new JLabel("79.05 kg");
		GridBagConstraints gbc_lblKg = new GridBagConstraints();
		gbc_lblKg.insets = new Insets(0, 0, 5, 5);
		gbc_lblKg.gridx = 2;
		gbc_lblKg.gridy = 4;
		panel_Info.add(lblKg, gbc_lblKg);
		
		JLabel lblNewLabel_10 = new JLabel("2.03 m");
		GridBagConstraints gbc_lblNewLabel_10 = new GridBagConstraints();
		gbc_lblNewLabel_10.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_10.gridx = 3;
		gbc_lblNewLabel_10.gridy = 4;
		panel_Info.add(lblNewLabel_10, gbc_lblNewLabel_10);
		
		JLabel lblNewLabel_11 = new JLabel("Typ");
		lblNewLabel_11.setFont(new Font("Tahoma", Font.PLAIN, 9));
		GridBagConstraints gbc_lblNewLabel_11 = new GridBagConstraints();
		gbc_lblNewLabel_11.insets = new Insets(0, 0, 0, 5);
		gbc_lblNewLabel_11.gridx = 1;
		gbc_lblNewLabel_11.gridy = 5;
		panel_Info.add(lblNewLabel_11, gbc_lblNewLabel_11);
		
		JLabel lblNewLabel_12 = new JLabel("Gewicht");
		lblNewLabel_12.setFont(new Font("Tahoma", Font.PLAIN, 9));
		GridBagConstraints gbc_lblNewLabel_12 = new GridBagConstraints();
		gbc_lblNewLabel_12.insets = new Insets(0, 0, 0, 5);
		gbc_lblNewLabel_12.gridx = 2;
		gbc_lblNewLabel_12.gridy = 5;
		panel_Info.add(lblNewLabel_12, gbc_lblNewLabel_12);
		
		JLabel lblNewLabel_13 = new JLabel("Gr\u00F6\u00DFe");
		lblNewLabel_13.setFont(new Font("Tahoma", Font.PLAIN, 9));
		GridBagConstraints gbc_lblNewLabel_13 = new GridBagConstraints();
		gbc_lblNewLabel_13.insets = new Insets(0, 0, 0, 5);
		gbc_lblNewLabel_13.gridx = 3;
		gbc_lblNewLabel_13.gridy = 5;
		panel_Info.add(lblNewLabel_13, gbc_lblNewLabel_13);
		
		Panel panel_PowerUp = new Panel();
		panel_PowerUp.setBackground(Color.WHITE);
		GridBagConstraints gbc_panel_PowerUp = new GridBagConstraints();
		gbc_panel_PowerUp.fill = GridBagConstraints.BOTH;
		gbc_panel_PowerUp.insets = new Insets(0, 0, 5, 0);
		gbc_panel_PowerUp.gridx = 0;
		gbc_panel_PowerUp.gridy = 1;
		panel.add(panel_PowerUp, gbc_panel_PowerUp);
		GridBagLayout gbl_panel_PowerUp = new GridBagLayout();
		gbl_panel_PowerUp.columnWidths = new int[]{47, 0, 46, 46, 0, 0};
		gbl_panel_PowerUp.rowHeights = new int[]{0, 0, 0, 11, 0, 0, 0};
		gbl_panel_PowerUp.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_panel_PowerUp.rowWeights = new double[]{0.0, 0.0, 1.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		panel_PowerUp.setLayout(gbl_panel_PowerUp);
		
		JLabel lblNewLabel_14 = new JLabel("66726");
		lblNewLabel_14.setFont(new Font("Tahoma", Font.PLAIN, 12));
		GridBagConstraints gbc_lblNewLabel_14 = new GridBagConstraints();
		gbc_lblNewLabel_14.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_14.gridx = 1;
		gbc_lblNewLabel_14.gridy = 1;
		panel_PowerUp.add(lblNewLabel_14, gbc_lblNewLabel_14);
		
		JLabel lblNewLabel_16 = new JLabel("26");
		lblNewLabel_16.setFont(new Font("Tahoma", Font.PLAIN, 12));
		GridBagConstraints gbc_lblNewLabel_16 = new GridBagConstraints();
		gbc_lblNewLabel_16.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_16.gridx = 3;
		gbc_lblNewLabel_16.gridy = 1;
		panel_PowerUp.add(lblNewLabel_16, gbc_lblNewLabel_16);
		
		JLabel lblNewLabel_17 = new JLabel("STERNSTAUB");
		lblNewLabel_17.setFont(new Font("Tahoma", Font.PLAIN, 8));
		GridBagConstraints gbc_lblNewLabel_17 = new GridBagConstraints();
		gbc_lblNewLabel_17.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_17.gridx = 1;
		gbc_lblNewLabel_17.gridy = 2;
		panel_PowerUp.add(lblNewLabel_17, gbc_lblNewLabel_17);
		
		JLabel lblNewLabel_18 = new JLabel("BONBON");
		lblNewLabel_18.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_18.setFont(new Font("Tahoma", Font.PLAIN, 8));
		GridBagConstraints gbc_lblNewLabel_18 = new GridBagConstraints();
		gbc_lblNewLabel_18.fill = GridBagConstraints.BOTH;
		gbc_lblNewLabel_18.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_18.gridx = 3;
		gbc_lblNewLabel_18.gridy = 2;
		panel_PowerUp.add(lblNewLabel_18, gbc_lblNewLabel_18);
		
		Component verticalStrut_2 = Box.createVerticalStrut(1);
		GridBagConstraints gbc_verticalStrut_2 = new GridBagConstraints();
		gbc_verticalStrut_2.insets = new Insets(0, 0, 5, 5);
		gbc_verticalStrut_2.gridx = 1;
		gbc_verticalStrut_2.gridy = 3;
		panel_PowerUp.add(verticalStrut_2, gbc_verticalStrut_2);
		GridBagConstraints gbc_btnNewButton_2 = new GridBagConstraints();
		gbc_btnNewButton_2.insets = new Insets(0, 0, 5, 5);
		gbc_btnNewButton_2.gridx = 1;
		gbc_btnNewButton_2.gridy = 4;
		panel_PowerUp.add(btnNewButton_2, gbc_btnNewButton_2);
		
		JLabel lblNewLabel_15 = new JLabel("600");
		GridBagConstraints gbc_lblNewLabel_15 = new GridBagConstraints();
		gbc_lblNewLabel_15.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_15.gridx = 2;
		gbc_lblNewLabel_15.gridy = 4;
		panel_PowerUp.add(lblNewLabel_15, gbc_lblNewLabel_15);
		
		JLabel label = new JLabel("1");
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.insets = new Insets(0, 0, 5, 5);
		gbc_label.gridx = 3;
		gbc_label.gridy = 4;
		panel_PowerUp.add(label, gbc_label);
		
		JPanel panel_Attacken = new JPanel();
		panel_Attacken.setBackground(Color.WHITE);
		GridBagConstraints gbc_panel_Attacken = new GridBagConstraints();
		gbc_panel_Attacken.insets = new Insets(0, 0, 5, 0);
		gbc_panel_Attacken.fill = GridBagConstraints.BOTH;
		gbc_panel_Attacken.gridx = 0;
		gbc_panel_Attacken.gridy = 2;
		panel.add(panel_Attacken, gbc_panel_Attacken);
		GridBagLayout gbl_panel_Attacken = new GridBagLayout();
		gbl_panel_Attacken.columnWidths = new int[]{0, 75, 155, 0, 0, 0};
		gbl_panel_Attacken.rowHeights = new int[]{0, 20, 12, 20, 0, 0, 0, 0};
		gbl_panel_Attacken.columnWeights = new double[]{0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_panel_Attacken.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel_Attacken.setLayout(gbl_panel_Attacken);
		
		Component verticalStrut = Box.createVerticalStrut(1);
		GridBagConstraints gbc_verticalStrut = new GridBagConstraints();
		gbc_verticalStrut.insets = new Insets(0, 0, 5, 5);
		gbc_verticalStrut.gridx = 2;
		gbc_verticalStrut.gridy = 0;
		panel_Attacken.add(verticalStrut, gbc_verticalStrut);
		
		JLabel lblNewLabel_1 = new JLabel("Stahlfl\u00FCgel");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 1;
		gbc_lblNewLabel_1.gridy = 1;
		panel_Attacken.add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		JLabel lblNewLabel_5 = new JLabel("15");
		lblNewLabel_5.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblNewLabel_5 = new GridBagConstraints();
		gbc_lblNewLabel_5.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_5.gridx = 3;
		gbc_lblNewLabel_5.gridy = 1;
		panel_Attacken.add(lblNewLabel_5, gbc_lblNewLabel_5);
		GridBagConstraints gbc_horizontalStrut_1 = new GridBagConstraints();
		gbc_horizontalStrut_1.insets = new Insets(0, 0, 5, 0);
		gbc_horizontalStrut_1.gridx = 4;
		gbc_horizontalStrut_1.gridy = 1;
		panel_Attacken.add(horizontalStrut_1, gbc_horizontalStrut_1);
		
		JLabel lblNewLabel_3 = new JLabel("Stahl");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.PLAIN, 9));
		GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
		gbc_lblNewLabel_3.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_3.gridx = 1;
		gbc_lblNewLabel_3.gridy = 2;
		panel_Attacken.add(lblNewLabel_3, gbc_lblNewLabel_3);
		
		Component horizontalStrut = Box.createHorizontalStrut(1);
		GridBagConstraints gbc_horizontalStrut = new GridBagConstraints();
		gbc_horizontalStrut.gridheight = 2;
		gbc_horizontalStrut.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut.gridx = 0;
		gbc_horizontalStrut.gridy = 3;
		panel_Attacken.add(horizontalStrut, gbc_horizontalStrut);
		
		JLabel lblNewLabel_2 = new JLabel("Hyperstrahl");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 1;
		gbc_lblNewLabel_2.gridy = 4;
		panel_Attacken.add(lblNewLabel_2, gbc_lblNewLabel_2);
		
		JLabel lblNewLabel_6 = new JLabel("70");
		lblNewLabel_6.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblNewLabel_6 = new GridBagConstraints();
		gbc_lblNewLabel_6.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_6.gridx = 3;
		gbc_lblNewLabel_6.gridy = 4;
		panel_Attacken.add(lblNewLabel_6, gbc_lblNewLabel_6);
		
		JLabel lblNewLabel_4 = new JLabel("Normal");
		lblNewLabel_4.setFont(new Font("Tahoma", Font.PLAIN, 9));
		GridBagConstraints gbc_lblNewLabel_4 = new GridBagConstraints();
		gbc_lblNewLabel_4.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_4.gridx = 1;
		gbc_lblNewLabel_4.gridy = 5;
		panel_Attacken.add(lblNewLabel_4, gbc_lblNewLabel_4);
		
		Component verticalStrut_1 = Box.createVerticalStrut(1);
		GridBagConstraints gbc_verticalStrut_1 = new GridBagConstraints();
		gbc_verticalStrut_1.insets = new Insets(0, 0, 0, 5);
		gbc_verticalStrut_1.gridx = 2;
		gbc_verticalStrut_1.gridy = 6;
		panel_Attacken.add(verticalStrut_1, gbc_verticalStrut_1);
		
		JPanel panel_Fangdatum = new JPanel();
		panel_Fangdatum.setBackground(Color.WHITE);
		GridBagConstraints gbc_panel_Fangdatum = new GridBagConstraints();
		gbc_panel_Fangdatum.insets = new Insets(0, 0, 5, 0);
		gbc_panel_Fangdatum.fill = GridBagConstraints.BOTH;
		gbc_panel_Fangdatum.gridx = 0;
		gbc_panel_Fangdatum.gridy = 3;
		panel.add(panel_Fangdatum, gbc_panel_Fangdatum);
		
		JLabel lblNewLabel = new JLabel("26.07.2016");
		panel_Fangdatum.add(lblNewLabel);
		
		JPanel panel_Option = new JPanel();
		panel_Option.setBackground(Color.WHITE);
		GridBagConstraints gbc_panel_Option = new GridBagConstraints();
		gbc_panel_Option.fill = GridBagConstraints.BOTH;
		gbc_panel_Option.gridx = 0;
		gbc_panel_Option.gridy = 4;
		panel.add(panel_Option, gbc_panel_Option);
		GridBagLayout gbl_panel_Option = new GridBagLayout();
		gbl_panel_Option.columnWidths = new int[]{0, 0, 0, 0};
		gbl_panel_Option.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_panel_Option.columnWeights = new double[]{1.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_panel_Option.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel_Option.setLayout(gbl_panel_Option);
		
		JButton btnNewButton = new JButton("Verschicken");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		
		Component verticalStrut_4 = Box.createVerticalStrut(5);
		GridBagConstraints gbc_verticalStrut_4 = new GridBagConstraints();
		gbc_verticalStrut_4.insets = new Insets(0, 0, 5, 5);
		gbc_verticalStrut_4.gridx = 1;
		gbc_verticalStrut_4.gridy = 0;
		panel_Option.add(verticalStrut_4, gbc_verticalStrut_4);
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.insets = new Insets(0, 0, 5, 5);
		gbc_btnNewButton.gridx = 1;
		gbc_btnNewButton.gridy = 1;
		panel_Option.add(btnNewButton, gbc_btnNewButton);
		
		JButton btnNewButton_1 = new JButton("X");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		
		Component verticalStrut_3 = Box.createVerticalStrut(5);
		GridBagConstraints gbc_verticalStrut_3 = new GridBagConstraints();
		gbc_verticalStrut_3.insets = new Insets(0, 0, 5, 5);
		gbc_verticalStrut_3.gridx = 1;
		gbc_verticalStrut_3.gridy = 2;
		panel_Option.add(verticalStrut_3, gbc_verticalStrut_3);
		GridBagConstraints gbc_btnNewButton_1 = new GridBagConstraints();
		gbc_btnNewButton_1.fill = GridBagConstraints.VERTICAL;
		gbc_btnNewButton_1.insets = new Insets(0, 0, 0, 5);
		gbc_btnNewButton_1.gridx = 1;
		gbc_btnNewButton_1.gridy = 3;
		panel_Option.add(btnNewButton_1, gbc_btnNewButton_1);

	}

}
