
public class Attacke {

	String attackenname;
	int schaden;
	
	public Attacke(String attackenname, int schaden) {
		super();
		this.attackenname = attackenname;
		this.schaden = schaden;
	}

	public String getAttackenname() {
		return attackenname;
	}

	public void setAttackenname(String attackenname) {
		this.attackenname = attackenname;
	}

	public int getSchaden() {
		return schaden;
	}

	public void setSchaden(int schaden) {
		this.schaden = schaden;
	}

	@Override
	public String toString() {
		return "Attacke [attackenname=" + attackenname + ", schaden=" + schaden + "]";
	}	
}
