import java.io.File;
import java.time.LocalDate;

public class Pokemon {

	int nummer, wp, kp, maxkp, bonbons;
	String name, typ;
	File bild;
	boolean favorit;
	double gewicht, groesse;
	Attacke[] attacken;
	LocalDate funddatum;
	
	public Pokemon() {
		// TODO Auto-generated constructor stub
	}

	public Pokemon(int nummer, int wp, int kp, int maxkp, int bonbons, String name, String typ, File bild,
			boolean favorit, double gewicht, double groesse, Attacke[] attacken, LocalDate funddatum) {
		super();
		this.nummer = nummer;
		this.wp = wp;
		this.kp = kp;
		this.maxkp = maxkp;
		this.bonbons = bonbons;
		this.name = name;
		this.typ = typ;
		this.bild = bild;
		this.favorit = favorit;
		this.gewicht = gewicht;
		this.groesse = groesse;
		this.attacken = attacken;
		this.funddatum = funddatum;
	}

	public int getNummer() {
		return nummer;
	}

	public void setNummer(int nummer) {
		this.nummer = nummer;
	}

	public int getWp() {
		return wp;
	}

	public void setWp(int wp) {
		this.wp = wp;
	}

	public int getKp() {
		return kp;
	}

	public void setKp(int kp) {
		this.kp = kp;
	}

	public int getMaxkp() {
		return maxkp;
	}

	public void setMaxkp(int maxkp) {
		this.maxkp = maxkp;
	}

	public int getBonbons() {
		return bonbons;
	}

	public void setBonbons(int bonbons) {
		this.bonbons = bonbons;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public File getBild() {
		return bild;
	}

	public void setBild(File bild) {
		this.bild = bild;
	}

	public boolean isFavorit() {
		return favorit;
	}

	public void setFavorit(boolean favorit) {
		this.favorit = favorit;
	}

	public double getGewicht() {
		return gewicht;
	}

	public void setGewicht(double gewicht) {
		this.gewicht = gewicht;
	}

	public double getGroesse() {
		return groesse;
	}

	public void setGroesse(double groesse) {
		this.groesse = groesse;
	}

	public Attacke[] getAttacken() {
		return attacken;
	}

	public void setAttacken(Attacke[] attacken) {
		this.attacken = attacken;
	}

	public LocalDate getFunddatum() {
		return funddatum;
	}

	public void setFunddatum(LocalDate funddatum) {
		this.funddatum = funddatum;
	}
}
