import java.util.Random;

public abstract class Listengenerator {
	public static long[] erstelleListe(int laenge) {
		long[] liste = new long[laenge];
		Random rand = new Random();
		long schritt=Long.MAX_VALUE/laenge;
		for(int i=0;i<liste.length;i++) {
			liste[i]=i*schritt+rand.nextInt(Math.abs((int)schritt));
		}
		return liste;
	}
}
