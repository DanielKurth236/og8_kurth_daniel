import java.util.Scanner;

public class TestSuchalgorithmen {

	public static void main(String[] args) {
		Scanner scan=new Scanner(System.in);
		Stoppuhr uhr= new Stoppuhr();
		while(true) {
		System.out.print("l�nge der Liste: ");
		long [] liste=Listengenerator.erstelleListe(scan.nextInt());
		System.out.println("W�hle:\n0 best case\n1 avarage case\n2 worst case\n3 manuell");
		long nFuerLinear,nFuerBinaer;
		switch(scan.nextInt()) {
			case 0:
				nFuerLinear= liste[0];
				nFuerBinaer= liste[liste.length/2];
				break;
			case 1:
				nFuerLinear= liste[liste.length/2];
				nFuerBinaer= liste[(int)(Math.random()*liste.length)];
				break;
			case 3:
				System.out.print("Position eingeben: ");
				long n=liste[scan.nextInt()];
				nFuerLinear= n;
				nFuerBinaer= n;
				break;
			
			default:
				nFuerLinear= -1;
				nFuerBinaer= -1;
				
				
		}
		//Berechnung und Ausgabe f�r lineare Suche
		uhr.start();
		int pos=Suchalgorithmen.liniareSuche(liste, nFuerLinear);
		uhr.stopp();
		System.out.println("lineare Suche: "+uhr.getDauerInMs()+" ms");
		//Berechnung und Ausgabe f�r bin�re Suche
		uhr.start();
		Suchalgorithmen.binaereSuche(liste, nFuerBinaer);
		uhr.stopp();
		System.out.println("bin�re Suche: "+uhr.getDauerInMs()+" ms");
		//Berechnung und Ausgabe f�r fastsearch
		uhr.start();
		Suchalgorithmen.fastsearch(liste, nFuerBinaer);
		uhr.stopp();
		System.out.println("fastsearch: "+uhr.getDauerInMs()+" ms");
		/*//Ausgabe der Position und falls vorhanden des Wertes
		if(pos!=-1)
			System.out.println("liste["+pos+"]="+liste[pos]);
		else
			System.out.println(pos);*/
		}
		//scan.close();
	}

}
