public class Suchalgorithmen {

	public static int liniareSuche(long liste[], long gesuchteZahl) {
		for(int i = 0;i< liste.length; i++)
			if(gesuchteZahl == liste[i]){
				return i;	
			}
		return -1;		
	}
	
	public static int binaereSuche(long[] liste, long gesuchteZahl) {
		int bis = liste.length -1;
		int von = 0;
		while(von <= bis) {
			
			int median = von + ((bis - von) / 2);
			
			if(liste[median] == gesuchteZahl) {
				return median;
			}
			else {
				if(liste[median] > gesuchteZahl) {
					bis = median -1;
				}
				else {
					von = median + 1;	
				}
			}
			
		}
		return -1;
	}
	public static int fastsearch(long[] liste, long gesuchteZahl) {
		return fastsearch(liste, 0, liste.length-1, gesuchteZahl);
	}
	
	private static int fastsearch(long[] liste, int von, int bis, long gesuchteZahl) {
			
		if(von <= bis) {
			int m_b = (von + bis) / 2;
			int m_i = (int) (von + (bis - von) * ((gesuchteZahl - liste[von]) / (liste[bis] - liste[von])));
			
			if(m_b > m_i) {
				int m_i2 = m_i;
				m_b = m_i;
				m_i = m_i2;
			}else if(gesuchteZahl == liste[m_i]) {
				return m_i;			
			}else if(gesuchteZahl < liste[m_b]) {
				return fastsearch(liste, m_b+1,m_i-1, gesuchteZahl);
			}else if(gesuchteZahl < liste[m_i]) {
				return fastsearch(liste, m_b+1,m_i-1, gesuchteZahl);
			}else
				return fastsearch(liste, m_i+1, bis, gesuchteZahl);
			}
			
		return -1;
	}
	
}
