import java.util.Scanner;

public class QuotSub {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Bitte geben Sie einen Quotient ein: ");
		int quotient = sc.nextInt();
		
		System.out.println("Bitte geben Sie einen Dividend ein: ");
		int dividend = sc.nextInt();

		int count = 0;
		while(quotient <= dividend) {
			dividend = dividend - quotient;
			count++;
		}
		
		System.out.println("Berechnung des Quotienten und Rest ueber Subtraktion \n" + "Divisor: " + count + "\n"+ "Rest: " + dividend);	
		
	}

}
