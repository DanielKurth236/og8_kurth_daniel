
public class kgV {

	int z1 = 3;
	int z2 = 2;

	public static int kgVberechnen(int zahl1, int zahl2) {
		int temp1 = zahl1, temp2 = zahl2;
		if (zahl1 <= 0 || zahl2 <= 0) {
			return -1;
		}
		while (temp1 != temp2) {
			if (temp1 < temp2) {
				temp1 += zahl1;
			} else {
				temp2 += zahl2;
			}
		}
		

		return temp1;

	}

}
