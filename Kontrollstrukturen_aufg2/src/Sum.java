import java.util.Scanner;

public class Sum {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Grenzwert n: ");
		int n = sc.nextInt();
		
		int summe = 0;
		for(int i = 1; i <= n; i++) {
			summe = summe + (2 * i);
		}
		System.out.println("Ergebnis: " + summe);
	}

}
