import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.CardLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.JComboBox;

public class CardLayoutExample extends JFrame {

	private JPanel contentPane;
	private CardLayout card;
	JPanel panel = new JPanel();
	JPanel panel_1 = new JPanel();
	JPanel panel_2 = new JPanel();
	JPanel panel_cardLayout = new JPanel();
	JPanel panel_start = new JPanel();
	JPanel panel_3 = new JPanel();
	JPanel panel_4 = new JPanel();
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CardLayoutExample frame = new CardLayoutExample();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CardLayoutExample() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		card = new CardLayout();
		
		panel.setBackground(Color.LIGHT_GRAY);
		contentPane.add(panel, BorderLayout.NORTH);
		
		JButton Button1 = new JButton("green");
		Button1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				card.show(panel_cardLayout,"panel_1");
			}
		});
		panel.add(Button1);
		
		
		JButton Button2 = new JButton("cyan");
		Button2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				card.show(panel_cardLayout,"panel_2");
			}
		});
		panel.add(Button2);
		
		
		JButton Button3 = new JButton("red");
		Button3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				card.show(panel_cardLayout,"panel_3");

			}
		});
		panel.add(Button3);
		
		
		JButton Button4 = new JButton("black");
		Button4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				card.show(panel_cardLayout,"panel_4");
			}
		});
		panel.add(Button4);
		
		
		JButton Button5 = new JButton("Next");
		Button5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				card.next(panel_cardLayout);
			}
		});
		panel.add(Button5);
		
		JButton Button6 = new JButton("Previous");
		Button6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				card.previous(panel_cardLayout);
			}
		});
		panel.add(Button6);
		
		//ComboBox
		JComboBox<String> comboBox = new JComboBox<String>();
		panel.add(comboBox);
		comboBox.addItem("green");
		comboBox.addItem("cyan");
		comboBox.addItem("red");
		comboBox.addItem("black");
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			String selected = (String) comboBox.getSelectedItem();
			if (selected.equals("green")) {
				card.show(panel_cardLayout, "panel_1");
			} else if (selected.equals("cyan")) {
				card.show(panel_cardLayout, "panel_2");

			} else if (selected.equals("red")) {
				card.show(panel_cardLayout, "panel_3");

			} else 
				card.show(panel_cardLayout, "panel_4");
			}
		});
		
		//contetPane
		contentPane.add(panel_cardLayout, BorderLayout.CENTER);
		panel_cardLayout.setLayout(card);	
		
		//start Panel
		panel_cardLayout.add(panel_start, "panel_start");
		
		//Panel 1
		panel_1.setBackground(Color.GREEN);
		panel_cardLayout.add(panel_1, "panel_1");
		
		//Panel 2
		panel_2.setBackground(Color.CYAN);
		panel_cardLayout.add(panel_2, "panel_2");
		
		//Panel 3
		panel_3.setBackground(Color.RED);
		panel_cardLayout.add(panel_3, "panel_3");
		
		//Panel 4
		panel_4.setBackground(Color.BLACK);
		panel_cardLayout.add(panel_4, "panel_4");
		
	}
}
