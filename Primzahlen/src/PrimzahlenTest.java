import java.util.Scanner;

public class PrimzahlenTest {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Stoppuhr st = new Stoppuhr();
		while(true) {
		System.out.println("Bitte geben Sie eine Zahl ein die gepr�ft werden soll:");		
		long zahl = sc.nextLong();															
		st.start();
		boolean ausgabe = isPrimzahl(zahl);
		st.stopp();
		System.out.println(ausgabe +" / Zeit: " + st.getDauerInMs() );	
		System.out.println();
		}
		
		
	}

	public static boolean isPrimzahl(long zahl) {						
		if (zahl >= 2) {																	 
			for (int i = 2; i < zahl; i++) {												
				if (zahl % i == 0)																
					return false;	
			}
			return true;																	
		}
		return false;																		 
	}

	
}
