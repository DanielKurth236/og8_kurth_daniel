import java.util.Scanner;

public class Prim {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Bitte geben Sie eine Zahl ein die gepr�ft werden soll:");
		long zahl = sc.nextLong();

		boolean prim = false;
		if (zahl >= 2) {
			for (int i = 2; i < zahl; i++) {
				if (zahl % i == 0)
					prim = false;
			}
			prim = true;
		}
		
		System.out.println(zahl + " = " + prim);
	}

}
