import java.util.ArrayList;

public class KeyStore02 {
	
	ArrayList<String> list = new ArrayList<String>();

	public boolean add(String eintrag) {
		list.add(eintrag);
		return true;
	}

	public int indexOf(String eintrag) {
		if(list.contains(eintrag))
		return list.indexOf(eintrag);
		return -1;
	}

	public void remove(int index) {
		list.remove(index);
	}

	@Override
	public String toString() {
		return "KeyStore02 [list=" + list + "]";
	}
	
	
}

