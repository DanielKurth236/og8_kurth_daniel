import java.util.Arrays;

public class KeyStore01 {
	static String[] aKeyStore = new String[1];
	static int counter = -1;

	public KeyStore01(String[] aKeyStore, int counter) {
		super();
		this.aKeyStore = aKeyStore;
		this.counter = counter;
	}

	public KeyStore01() {

	}

	public boolean add(String eintrag) {
		counter++;
		if (counter < aKeyStore.length) {
			this.aKeyStore[counter] = eintrag;

			return true;

		} else {
			String[] Array = new String[aKeyStore.length + 1];
			for (int i = 0; i < aKeyStore.length; i++) {
				Array[i] = aKeyStore[i];
			}
			Array[counter] = eintrag;

			aKeyStore = Array;
			this.aKeyStore[counter] = eintrag;
		}
		return false;
	}

	public int indexOf(String eingabe) {
		for (int i = 0; i < aKeyStore.length; i++)
			if (aKeyStore[i].equals(eingabe)) {
				return i;
			}
		return -1;
	}

	public boolean remove(int index) {
		
		for (int i = 0; i < aKeyStore.length; i++) {
			if (i ==index) {
				// shifting elements
				for (int j = i; j < aKeyStore.length - 1; j++) {
					aKeyStore[j] = aKeyStore[j + 1];
				}
				aKeyStore[aKeyStore.length-1]="";
				counter--; 
				return true;
			}
		}

		return false;
	}

	@Override
	public String toString() {
		return "KeyStore01 [aKeyStore=" + Arrays.toString(aKeyStore) + ", counter=" + counter + "]";
	}

}
