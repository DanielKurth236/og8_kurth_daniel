public class Addon {
  
  private String name;
  private double preis;
  private int idNummer;
  private int maxAnzahl;
  private int tatsaelicheAnzahl;
  
  public Addon(String name, double preis, int idNummer, int maxAnzahl, int tatsaelicheAnzahl) {
	super();
	this.name = name;
	this.preis = preis;
	this.idNummer = idNummer;
	this.maxAnzahl = maxAnzahl;
	this.tatsaelicheAnzahl = tatsaelicheAnzahl;
}

public String getName() {
    return name;
  }

  public void setName(String nameNeu) {
    name = nameNeu;
  }

  public double getPreis() {
    return preis;
  }

  public void setPreis(double preisNeu) {
    preis = preisNeu;
  }

  public int getIdNummer() {
    return idNummer;
  }

  public void setIdNummer(int idNummerNeu) {
    idNummer = idNummerNeu;
  }
  
  public int getMaxAnzahl() {
	  return maxAnzahl;
  }
  
  public void setMaxAnzahl(int maxAnzahlNeu) {
	maxAnzahl = maxAnzahlNeu; 
  }

  public int getTatsaelicheAnzahl() {
    return tatsaelicheAnzahl;
  }

  public void setTatsaelicheAnzahl(int tatsaelicheAnzahlNeu) {
    tatsaelicheAnzahl = tatsaelicheAnzahlNeu;
  }
  public int verbrauchen() {

	  return 0;
  }

  public int kaufen() {

	  return 0;
  }

  public int bestandswert() {

	  return 0;
  }

}

