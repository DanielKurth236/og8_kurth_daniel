import java.util.Arrays;

public class Mergesort {

	public static int[] mergesort(int[] A, int l, int r) {
		if (l < r) {
			System.out.println(Arrays.toString(A));
			int q = (l + r) / 2;

			mergesort(A, l, q);
			mergesort(A, q + 1, r);

			merge(A, l, q, r);
		}

		return A;
	}

	private static void merge(int[] A, int l, int q, int r) {
		int[] arr = new int[A.length];
		for (int i = l; i <= q; i++) {
			arr[i] = A[i];
		}
		for (int j = q + 1; j <= r; j++) {
			arr[r + q + 1 - j] = A[j];
		}
		int i = l;
		int j = r;
		for (int k = l; k <= r; k++) {
			if (arr[i] <= arr[j]) {
				A[k] = arr[i];
				i++;
			} else {
				A[k] = arr[j];
				j--;
			}
		}

	}
}
