import java.util.Arrays;

public class Bubblesort {

	public static int[] bubblesortmethode(int[] a) {
		int count = 0;
		for (int n = a.length; n >= 0; n--) {
			for (int i = 0; i < n-1; i++) {
				if (a[i] > a[i + 1]) {
					int temp = a[i + 1];
					a[i + 1] = a[i];
					a[i] = temp;
					count++;
				}
			}
			System.out.println(Arrays.toString(a));
		}
		return a;
	}
}
