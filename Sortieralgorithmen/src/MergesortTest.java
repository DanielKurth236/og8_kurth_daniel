import java.util.Arrays;

public class MergesortTest {

	public static void main(String[] args) {
		
		int[] intArr = { 12, 51, 16, 75, 31, 27, 45, 56, 98, 23, 45, 34, 97, 37, 64, 28, 65, 89, 94, 36, 38};

		System.out.println(Arrays.toString(Mergesort.mergesort(intArr, 0, intArr.length - 1)));
	}
}
