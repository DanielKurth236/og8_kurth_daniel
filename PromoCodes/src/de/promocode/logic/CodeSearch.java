package de.promocode.logic;

public class CodeSearch {

	public static int NOT_FOUND = -1;

	/**
	 * Sucht einen Code in der Liste.
	 * 
	 * @param list
	 *            Liste der Codes im Long-Format
	 * @param searchValue
	 *            Gesuchter Code
	 * @return Index des Codes, wenn er vorhanden ist, NOT_FOUND wenn er nicht
	 *         gefunden werden konnte
	 */
	public static int findPromoCode(long[] list, long searchValue) {
		int bis = list.length - 1;
		int von = 0;
		while (von <= bis) {

			int median = von + ((bis - von) / 2);

			if (list[median] == searchValue) {
				return median;
			} else {
				if (list[median] > searchValue) {
					bis = median - 1;
				} else {
					von = median + 1;
				}
			}

		}
		return NOT_FOUND;

	}

}
