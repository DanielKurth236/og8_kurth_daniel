import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Datei_I_O {
	
	private File file;

	public static void main(String[] args) {
		File file = new File("Primzahlen.txt");
		Datei_I_O dio = new Datei_I_O(file);
		
		dio.write("dfdsf");
	
	}
	
	public Datei_I_O(File file) {
		this.file = file;
	}
	
	public void write(String s) {
		try {
			FileWriter fw = new FileWriter(this.file, true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.newLine();
			bw.write(s);
			bw.flush();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("File konnte nicht gefunden werden / Path fehlerhaft");
		}
		
	}
	
	public void lesen() {
		try {
			FileReader fr = new FileReader(this.file);
			BufferedReader br = new BufferedReader(fr);
			String s;
			while((s = br.readLine()) != null){
				System.out.println(s);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("File konnte nicht gefunden werden / Path fehlerhaft");
		}
		
	}

}
