import java.util.Arrays;

public class radixSort {

	public static void main(String[] args) {

		int liste[] = { 3, 7, 12, 645, 978, 22, 534, 567 };
		int i = liste.length;
		radixsort(liste, i);
		System.out.println(Arrays.toString(liste));

	}

	static int maximum(int liste[], int i){

		int max = liste[0];
		for (int a = 1; a < i; a++)

			if (liste[a] > max)

				max = liste[a];

		return max;

	}

	static void countingsort(int liste[], int i, int faktor) {

		int ausgabe[] = new int[i];
		int a;
		int zaehlen[] = new int[10];
		Arrays.fill(zaehlen, 0);

		for (a = 0; a < i; a++) {

			zaehlen[(liste[a] / faktor) % 10]++;
		}

		for (a = 1; a < 10; a++) {

			zaehlen[a] += zaehlen[a - 1];

		}
		for (a = i - 1; a >= 0; a--) {

			ausgabe[zaehlen[(liste[a] / faktor) % 10] - 1] = liste[a];
			zaehlen[(liste[a] / faktor) % 10]--;

		}

		for (a = 0; a < i; a++)

			liste[a] = ausgabe[a];

	}

	static void radixsort(int liste[], int i) {

		int m = maximum(liste, i);

		for (int faktor = 1; m / faktor > 0; faktor *= 10)
			countingsort(liste, i, faktor);

	}
	
}
