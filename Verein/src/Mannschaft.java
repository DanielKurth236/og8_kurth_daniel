import java.util.Arrays;

public class Mannschaft {

	private String name;
	private Trainer trainer;
	private String spielklasse;
	private Spieler[] spieler = new Spieler[22];
	private Spiel[] spiel;

	public Mannschaft(String name, Trainer trainer, String spielklasse, Spieler[] spieler) {
		super();
		this.name = name;
		this.trainer = trainer;
		this.spielklasse = spielklasse;
		if (spieler.length < 11) {

		} else
			this.spieler = spieler;
	}

	public Spiel[] getSpiel() {
		return spiel;
	}

	public void setSpiel(Spiel[] spiel) {
		this.spiel = spiel;
	}

	public Spieler[] getSpieler() {
		return spieler;
	}

	public void setSpieler(Spieler[] spieler) {
		if (spieler.length < 11) {

		} else
			this.spieler = spieler;
	}

	public String getName() {
		return name;
	}

	public void setName(String nameNeu) {
		name = nameNeu;
	}

	public Trainer getTrainer() {
		return trainer;
	}

	public void setTrainer(Trainer trainerNeu) {
		trainer = trainerNeu;
	}

	public String getSpielklasse() {
		return spielklasse;
	}

	public void setSpielklasse(String spielklasseNeu) {
		spielklasse = spielklasseNeu;
	}

	@Override
	public String toString() {
		return "Mannschaft [name=" + name + ", trainer=" + trainer + ", spielklasse=" + spielklasse + ", spieler="
				+ Arrays.toString(spieler) + ", spiel=" + Arrays.toString(spiel) + "]";
	}

}
