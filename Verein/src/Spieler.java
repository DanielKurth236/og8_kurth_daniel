public class Spieler extends Mitglieder {

	private int trikotnummer;
	private String spielposition;
	private Mannschaft mannschaft;

	public Spieler(long telefonnummer, boolean jahresbeitragBezahlt, String name, int trikotnummer, String spielposition) {
		super(telefonnummer, jahresbeitragBezahlt, name);
		this.trikotnummer = trikotnummer;
		this.spielposition = spielposition;
	}


	public Mannschaft getMannschaft() {
		return mannschaft;
	}


	public void setMannschaft(Mannschaft mannschaft) {
		this.mannschaft = mannschaft;
	}


	public int getTrikotnummer() {
		return trikotnummer;
	}

	public void setTrikotnummer(int trikotnummerNeu) {
		trikotnummer = trikotnummerNeu;
	}

	public String getSpielposition() {
		return spielposition;
	}

	public void setSpielposition(String spielpositionNeu) {
		spielposition = spielpositionNeu;
	}

	@Override
	public String toString() {
		return "Spieler [trikotnummer=" + trikotnummer + ", spielposition=" + spielposition + ", mannschaft="
				+ mannschaft + "]";
	}

}