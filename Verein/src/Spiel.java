public class Spiel {

	private boolean heimMannschaft;
	private String wann;
	private int heimTore;
	private int gastTore;
	private Mannschaft mannschaft;

	public Spiel(boolean heimMannschaft, String wann, int heimTore, int gastTore, Mannschaft mannschaft) {
		super();
		this.heimMannschaft = heimMannschaft;
		this.wann = wann;
		this.heimTore = heimTore;
		this.gastTore = gastTore;
		this.mannschaft = mannschaft;
	}

	public boolean getHeimMannschaft() {
		return heimMannschaft;
	}

	public Mannschaft getMannschaft() {
		return mannschaft;
	}

	public void setMannschaft(Mannschaft mannschaft) {
		this.mannschaft = mannschaft;
	}

	public void setHeimMannschaft(boolean heimMannschaftNeu) {
		heimMannschaft = heimMannschaftNeu;
	}

	public String getWann() {
		return wann;
	}

	public void setWann(String wannNeu) {
		wann = wannNeu;
	}

	public int getHeimTore() {
		return heimTore;
	}

	public void setHeimTore(int heimToreNeu) {
		heimTore = heimToreNeu;
	}

	public int getGastTore() {
		return gastTore;
	}

	public void setGastTore(int gastToreNeu) {
		gastTore = gastToreNeu;
	}

}