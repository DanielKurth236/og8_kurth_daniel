public class Schiedsrichter extends Mitglieder {

	private int anzahlGepiffeneSpiele;
	

	public Schiedsrichter(long telefonnummer, boolean jahresbeitragBezahlt, String name, int anzahlGepiffeneSpiele) {
		super(telefonnummer, jahresbeitragBezahlt, name);
		this.anzahlGepiffeneSpiele = anzahlGepiffeneSpiele;
	}

	public Schiedsrichter(int anzahlGepiffeneSpiele) {
		this.anzahlGepiffeneSpiele = anzahlGepiffeneSpiele;
	}

	public int getAnzahlGepiffeneSpiele() {
		return anzahlGepiffeneSpiele;
	}

	public void setAnzahlGepiffeneSpiele(int anzahlGepiffeneSpieleNeu) {
		anzahlGepiffeneSpiele = anzahlGepiffeneSpieleNeu;
	}

}
