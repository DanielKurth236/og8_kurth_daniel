public class Mitglieder {

	private String name;
	private long telefonnummer;
	private boolean jahresbeitragBezahlt;

	public Mitglieder(long telefonnummer, boolean jahresbeitragBezahlt, String name) {
		super();
		this.name = name;
		this.telefonnummer = telefonnummer;
		this.jahresbeitragBezahlt = jahresbeitragBezahlt;
	}

	public Mitglieder() {
	}

	public void setName(String nameNeu) {
		name = nameNeu;
	}
	
	public String getName() {
		return name;
	}

	public long getTelefonnummer() {
		return telefonnummer;
	}

	public void setTelefonnummer(long telefonnummerNeu) {
		telefonnummer = telefonnummerNeu;
	}

	public boolean getJahresbeitragBezahlt() {
		return jahresbeitragBezahlt;
	}

	public void setJahresbeitragBezahlt(boolean jahresbeitragBezahltNeu) {
		jahresbeitragBezahlt = jahresbeitragBezahltNeu;
	}

}
