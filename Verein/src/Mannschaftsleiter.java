public class Mannschaftsleiter extends Spieler {

	private String mannschaftsName;
	private int rabatt;

	public Mannschaftsleiter(long telefonnummer, boolean jahresbeitragBezahlt, String name, int trikotnummer, String spielposition, String mannschaftsName, int rabatt) {
		super(telefonnummer, jahresbeitragBezahlt, name, trikotnummer, spielposition);
		this.mannschaftsName = mannschaftsName;
		this.rabatt = rabatt;
	}

	public String getMannschaftsName() {
		return mannschaftsName;
	}

	public void setMannschaftsName(String mannschaftsNameNeu) {
		mannschaftsName = mannschaftsNameNeu;
	}

	public int getRabatt() {
		return rabatt;
	}

	public void setRabatt(int rabattNeu) {
		rabatt = rabattNeu;
	}

}
