import java.util.Arrays;

public class Trainer extends Mitglieder {

	private char lizensklasse;
	private int aufwandentschaedigung;
	private Mannschaft[] mannschaften;

	public Trainer(long telefonnummer, boolean jahresbeitragBezahlt, String name, char lizensklasse, int aufwandentschaedigung) {
		super(telefonnummer, jahresbeitragBezahlt, name);
		this.lizensklasse = lizensklasse;
		this.aufwandentschaedigung = aufwandentschaedigung;
	}

	public Mannschaft[] getMannschaften() {
		return mannschaften;
	}

	public void setMannschaften(Mannschaft[] mannschaften) {
		this.mannschaften = mannschaften;
	}

	public char getLizensklasse() {
		return lizensklasse;
	}

	public void setLizensklasse(char lizensklasseNeu) {
		lizensklasse = lizensklasseNeu;
	}

	public int getAufwandentschaedigung() {
		return aufwandentschaedigung;
	}

	public void setAufwandentschaedigung(int aufwandentschaedigungNeu) {
		aufwandentschaedigung = aufwandentschaedigungNeu;
	}

	@Override
	public String toString() {
		return "Trainer [lizensklasse=" + lizensklasse + ", aufwandentschaedigung=" + aufwandentschaedigung
				+ ", mannschaften=" + Arrays.toString(mannschaften) + "]";
	}

	
}
