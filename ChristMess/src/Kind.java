
public class Kind implements Comparable<Kind>{
	//Jedes Kind hat einen Nachnamen, Vornamen, Geburtstag, Wohnort und Bravheitsgrad
	//Erstellen Sie einen vollparametrisierten Konstruktor, Getter/Setter und eine toString-Methode

	String vorname, nachname, ort, geburtsdatum;
	int bravheitsgrad;
	
	public Kind(String vorname, String nachname, String geburtsdatum, int bravheitsgrad, String ort) {
		super();
		this.vorname = vorname;
		this.nachname = nachname;
		this.ort = ort;
		this.geburtsdatum = geburtsdatum;
		this.bravheitsgrad = bravheitsgrad;
	}
	

	public Kind() {
		super();
	}


	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}

	public String getGeburtsdatum() {
		return geburtsdatum;
	}

	public void setGeburtsdatum(String geburtsdatum) {
		this.geburtsdatum = geburtsdatum;
	}

	public int getBravheitsgrad() {
		return bravheitsgrad;
	}

	public void setBravheitsgrad(int bravheitsgrad) {
		this.bravheitsgrad = bravheitsgrad;
	}
	
	@Override
	public int compareTo(Kind o) {
		if(getBravheitsgrad() != o.bravheitsgrad) {
			return o.bravheitsgrad - bravheitsgrad; 
		}
		return getOrt().compareTo(o.ort);
	}

	@Override
	public String toString() {
		return "Kind [vorname=" + vorname + ", nachname=" + nachname + ", ort=" + ort + ", geburtsdatum=" + geburtsdatum
				+ ", bravheitsgrad=" + bravheitsgrad + "]";
	}
}
