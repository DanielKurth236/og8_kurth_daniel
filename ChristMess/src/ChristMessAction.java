
import java.util.*;

public class ChristMessAction {

	public static void main(String[] args) {

		
		KindGenerator KG = new KindGenerator();
		KG.generateKinderListe();
		Collections.sort(KG.kinderListe);
		System.out.println("Liste aller Kinder sortiert nach Bravheitsgrad und Ort:");
		System.out.println(KG.kinderListe.toString());
		
		System.out.println("Liste aller Kinder sortiert nach Bravheitsgrad und Ort, wenn Bravheitsgrad > 0");
		for(Kind k : KG.kinderListe) {
			if(k.getBravheitsgrad() > 0)
				System.out.println(k);
		}
		return;
		
	}

}
