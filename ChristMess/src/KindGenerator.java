import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;

public class KindGenerator{
	
	//Hier eine Methode erstellen, um die Datei der Kinder auszulesen und mit den Daten eine Liste an Kind-Objekte anlegen
	
	ArrayList<Kind> kinderListe = new ArrayList<Kind>();
	
	private File file = new File("kinddaten.txt");
	
	public KindGenerator() {
		super();
	}

	public KindGenerator(ArrayList<Kind> kinderListe, File file) {
		super();
		this.kinderListe = kinderListe;
		this.file = file;
	}

	public ArrayList<Kind> generateKinderListe() {
		try {
			FileReader fr = new FileReader(this.file);
			BufferedReader br = new BufferedReader(fr);
			String s;
			while((s = br.readLine()) != null){
				Kind k = kindhinzufuegen(s);
				kinderListe.add(k);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("File konnte nicht gefunden werden / Path fehlerhaft");
		}
		
		return kinderListe;
	}
	
	public Kind kindhinzufuegen(String a) {
		String s = a.replace(",", "");
		String[] liste = s.split(" ");
		try {
		for (int i = 5; i < liste.length; i++) {
			liste[4] += liste[i];
		}
		return new Kind(liste[0], liste[1], liste[2], Integer.parseInt(liste[3]), liste[4]);
		}catch(Exception e) {
			System.out.println(a);
			e.printStackTrace();
			return null;
		}
	}

	
}