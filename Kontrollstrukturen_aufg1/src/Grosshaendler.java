import java.util.Scanner;

public class Grosshaendler {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Wie viele: ");
		int stueck = sc.nextInt();
		
		System.out.println("Preis pro Stueck: ");
		double preis = sc.nextDouble();
		
		
		double rechnungsbeitrag = stueck * preis;
		if(stueck < 10) {
			rechnungsbeitrag += 10;
		}
		rechnungsbeitrag *= 1.19;
		System.out.println("Rechnugsbeitrag: " + Math.round(100. * rechnungsbeitrag)/100.);
	}

}
