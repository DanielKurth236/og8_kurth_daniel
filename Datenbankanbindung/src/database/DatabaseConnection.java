package database;

import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.Connection;


public class DatabaseConnection {
	private String driver = "com.mysql.jdbc.Driver";
	private String url = "jdbc:mysql://localhost/keks";
	private String user = "root";
	private String passwort = "";
	
	public boolean insertKekse(String bezeichnung, String sorte) {
		String sql = "INSERT INTO t_kekse (bezeichnung, sorte) VALUES ('"+bezeichnung+"', '"+sorte+"');";
		
		try {
			//JDBC-Treiber laden
			Class.forName(driver);
			//Verbindung aufbauen
			Connection con = DriverManager.getConnection(url, user, passwort);
			
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		
		return true;
	}
	
	public static void main(String[] args) {
	 DatabaseConnection dbc = new DatabaseConnection();
	 dbc.insertKekse("gef�llter Keks", "Kokoskeks");
	 dbc.insertKekse("leerer Keks", "Schokokeks");
	 dbc.insertKekse("voller Keks", "Kokoskeks");
	 dbc.insertKekse("Keks", "Haselnusskeks");
	 dbc.insertKekse("gef�llter Keks", "Vanillekeks");
	 dbc.insertKekse("trockener Keks", "Pl�tzchen");
	}
}
